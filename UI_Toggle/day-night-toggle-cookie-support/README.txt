A Pen created at CodePen.io. You can find this one at https://codepen.io/michaelkurz_/pen/rYKPyq.

 A simple jQuery toggle to switch to Day/Night mode.

It adds a class to <body>, so you can customize it easily with CSS.