var theme = window.localStorage.currentTheme;

        $('body').addClass(theme);

        if ($("body").hasClass("light")) {
            $('.dntoggle').addClass('sun');
            $('.dntoggle').removeClass('moon');
        } else {
            $('.dntoggle').removeClass('sun');
            $('.dntoggle').addClass('moon');
        }

        $('.dntoggle').click(function() {
            $('.dntoggle').toggleClass('sun');
            $('.dntoggle').toggleClass('moon');

            if ($("body").hasClass("light")) {
                $('body').toggleClass('light');
                localStorage.removeItem('currentTheme');
                localStorage.currentTheme = "dark";
            } else {
                $('body').toggleClass('light');
                localStorage.removeItem('currentTheme');
                localStorage.currentTheme = "light";
            }
        });