'use strict';

var articlePosition = document.getElementById('content').offsetTop;

function fadeImg() {
  if (pageYOffset > 0) {
    var opacity = pageYOffset / articlePosition;

    /*
    console.log(`pageYOffset is ${pageYOffset}`);
    console.log(`Article position is ${articlePosition}`);
    console.log(`Opacity is ${opacity}`);
    console.log('---');
    */

    document.body.style.background = 'linear-gradient(rgba(255, 255, 255, ' + opacity + '), rgba(255, 255, 255, ' + opacity + ')), url(https://s3.amazonaws.com/www.michaelkurz.me/img/case_studies/chat/bb8.jpg';
  }
}

window.addEventListener('scroll', fadeImg);