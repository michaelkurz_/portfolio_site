<!DOCTYPE html>
<html lang="en" >
<head>

     <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109506120-1"></script>
      <script>
         window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-109506120-1');
      </script>
  
  <title>Writing Copy for Conversational Interfaces</title>
  <meta charset="UTF-8">
  <meta name="format-detection" content="telephone=no" />
      <meta name="msapplication-tap-highlight" content="no" />
      <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width" />
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  <link rel="icon" type="image/png" href="img/icons/mk_favicon.png">

      <link rel="stylesheet" href="css/article_style.css">

  
</head>

<body>
  <div class="container">
  <header class="head">
    <h1 class="head__title">Writing Copy for Conversational Interfaces</h1>
    
  </header>

  <article class="content" id="content">
    <h4 class="content__para">Before We Jump In:</h4>
    <br>
    <br>
    <p class="content__para">At this point, I’m sure most of us are familiar with design systems like bootstrap or foundation and the grids that provide their structure. (If not, you can can read more about them <a href="https://getbootstrap.com/docs/4.0/layout/grid/" target="_blank">here</a> and <a href="https://foundation.zurb.com/sites/docs/xy-grid.html" target="_blank">here</a>.) These grids are tried and true examples to follow so a perfectly legitimate question is: “Why reinvent the wheel for the Opus design System?”
    <br>
    <br>
    For the Opus team, that answer was a no brainer. At its core, Opus is about live music. More specifically, the artists who produce it, the businesses who value it and the audience that lives for it. With that in mind, when we began developing our design system it was clear that live music needed to be at its core as well.
    <br>
    <br>
    The conversation about putting music at the core of Symphony (our working title) started and ended with the number four (4). Simply put, four is pretty significant in music. Without delving deeply into music theory, here are a few examples:
    </p>
    <br>
    <ul class="content__list">
      <li>The 4/4 time signature also referred to as “common time” which is constructed of four beats per measure</li>
      <li>The major scale is built on two sets of four notes with first and last notes creating an octave interval (a pair of four relationship)</li>
      <li>The interval of a perfect fourth is a foundational element of many music genres (the tonic and subdominant relationship)</li>
      <li>The circle of fourths, a geometrical representation of relationships among the 12 pitch classes of the chromatic scale</li>
      <li>The typical number of movements in a symphony</li>
    </ul>
    <br>
    <p class="content__para">It was pretty clear that 4 needed to be the foundation of our design system.</p>
    <br>
    <br>
    <h4 class="content__para">Getting Started:</h4>
    <br>   
    <p class="content__para">With our base number in hand, it was time to jump into the fun part, the math! (said no one ever).
    </p>
    <br>
    <img class="content__img" src="https://s3.amazonaws.com/www.michaelkurz.me/img/case_studies/grid/equaition.jpg" width="600px" alt="Math Equation">
    <p class="photo_caption"><i>Good luck with that one kid.</i></p>
    <br>
    <p class="content__para">
    To start, the team decided that the cornerstone of our grid would be typography and set out to determine what the size of our body/paragraph text would be. With our “holy number” (4) as a reference we landed on 16px body text with our reasoning being:
    </p>
    <ul class="content__list">
      <li>Divisible by four</li>
      <li>Is four squared</li>
      <li>4/4 time signature or “common time”</li>
      <li>16 px is the default font size in many browsers</li>
      <li>Follows a 1:2 modular scale (geometric progression)</li>
    </ul>
    <br>
    <p class="content__para"><i>*Note: We used a bottom up methodology for creating our grid. A second option could have been a top down methodology where we determined our max grid width and worked down to our gutter and column sizes</i></p>
    <br>
    <br>
    <h4 class="content__para">Setting Vertical Rhythm</h4>
    <br>   
    <p class="content__para">With our default font size set, we moved on to calculating our line height and resulting Vertical Rhythm of our pages. Keeping our “holy number” in mind we landed at a 24px line height:
    <br>
    <br>
    16px * 1.5 = 24px
    <br>
    <br>
    <i>*Note: We referred to classic typographic theory when determining out line height and picked a multiplier that coincided with those principles while fitting with our “everything divisible by four” guideline</i></p>
    <br>
    <br>
    <h4 class="content__para">Translating Vertical Rhythm to the Grid:</h4>
    <br>
    <br>
    <p class="content__para">Now that we had our base line height it was time to start building our grid. The team decided that we would use static gutters and outer gutters at .5 default gutter width which made the process of translating our Vertical Rhythm to our grid pretty simple:
    <br>
    <br>
    Base Line Height = Gutter Width (24px)
    <br>
    <br>
    <i>*Note: Outer Gutter Width = 12px.</i>
    </p>
    <br>
    <br>
    <h4 class="content__para">Determining Max Grid Width:</h4>
    <br>
    <br>
    <p class="content__para">Next up was determining the number of columns our max grid width would contain. At first the team had its heart set on a 16 column setup that divided by two at each breakpoint (16/8/4). After taking a closer look at the spectrum of common device sizes we decided having only three breakpoints would not have been an ideal setup and landed on the more conventional 12 col grid. 
    <br>
    <br>
    At this point we had most of our numbers in place:
    </p>
    <br>
    <ul class="content__list">
      <li>4: our foundational number all other numbers are divisible by</li>
      <li>16: our base body font size</li>
      <li>24: our base line height</li>
      <li>24: our fixed gutter width</li>
      <li>12: the number of columns in our grid</li>
      <li>12: our outer gutter width</li>
    </ul>
    <br>
    <br>
    <p class="content__para">But to really have our grid take shape, we needed to determine our max width and column widths. 
    <br>
    <br>
    To determine our column width we simply multiplied our gutter width by, you guessed it, four, and landed on 96px as our column width:
    <br>
    <br>
    24px * 4 = 96px
    <br>
    <br>
    From here, calculating our total grid width became pretty easy:
    <br>
    <br>
    Col width * # of Col + Gutter width * # of gutters + outer gutter width * 2
    <br>
    <br>
    or
    <br>
    <br>
    96 * 12 + 24 * 11 + 24 
    <br>
    <br>
    At long last ending up with a max grid width of:
    </p>
    <br>
    <p class="content__para" style="font-size: 20px;"><b>1440px</b></p>
    <br>
    <img class="content__img" src="https://s3.amazonaws.com/www.michaelkurz.me/img/case_studies/grid/1440_grid.png" width="600px" alt="1440px Grid">
    <p class="photo_caption"><i>1440px Grid</i></p>
    <br>
    <h4 class="content__para">Setting the Breakpoints:</h4>
    <br>
    <br>
    <p class="content__para">Now that we had our grid, it was time to determine where our breakpoints would fall. To help guide our decision we turned to data illustrating the most common screen resolutions on the market today and cross-referenced that with our own user data. This analysis resulted in our decision to create 4 breakpoints in addition to our max grid. These breaks would fall at the beginning (or bottom) of each resolution range and their max columns and widths would be divisible by 4. When it was all said and done the variations looked like this:</p>
    <br>
    <br>
    <img class="content__img" src="https://s3.amazonaws.com/www.michaelkurz.me/img/case_studies/grid/breakpoints.png" width="600px" alt="Breakpoints for various device sizes">
    <br>
    <br>
    <h4 class="content__para">You Might be Wondering:</h4>
    <br>   
    <p class="content__para">After reading, you may be thinking:</p>
    <br>
    <ol class="content__list">
      <li>After all of the work up front you landed on a common grid width of 1440. Was it really worth all that work and effort?</li>
      <li>Why are you using 12 columns across all screen resolutions? </li>
    </ol>
    <br>
    <p class="content__para">To answer was it worth it? We emphatically think yes. First, the resulting layout grid is something that is entirely our own and will form one of the foundations of the Symphony design system. Second, there were some team members who had never built their own grid before so going through each step was truly eye opening for them. Some of the principles they learned along the way will surely be applied to every project they do going forward. 
    <br>
    <br>
    Why we are using 12 columns all the time is a little tougher to answer definitively. The short answer is 12/4 = 3 which feels awkward being the only odd number in the entire layout system. That being said, we may find that using 12 columns on smaller screen resolutions isn’t ideal and we will have to revisit the issue. 
    </p>
    <br>
    <br>
    <h4 class="content__para">Wrapping Up:</h4>
    <br>
    <br>
    <p class="content__para">With our grid now in place, the team is turning its attention to completing the style section of our design system before we start finalizing and documenting componentry and eventually refactoring code. </p>
    <br>
    <br>
    <h4 class="content__para">tl;dr:</h4>
    <br>   
    <p class="content__para">For anyone who jumped to the bottom of the page, here’s the cliffsnotes version of our process outlined above:</p>
    <br>
    <p class="content__para"><b>Setting the Scene:</b>
      <br>
      <br>
      When creating our design system we decided that music needed to be at its core. When deciding a number to be the foundation of our grid system, we landed on the number four (4) because of it's strong relationship with music.</p>
      <br>
      <br>
      <p class="content__para__hang">
      <b>Step1:</b> Determine baseline font size</p>
      <br>
      <br>
      <p class="content__para__hang">
      <b>Step2:</b> Use baseline font size to determine line height and vertical rhythm.</P>
      <br>
      <br>
      <p class="content__para__hang">
      <b>Step 3:</b> Take line height/vertical rhythm and translate it into grid with gutter width</P>
      <br>
      <br>
      <p class="content__para__hang">
      <b>Step 6:</b> Select the number of Columns to be used in grid</P>
      <br>
      <br>
      <p class="content__para__hang">
      <b>Step 4:</b> Use gutter width and foundation number to determin column width</P>
      <br>
      <br>
      <p class="content__para__hang">
      <b>Step 5:</b> Determine max grid width by totaling column widths, gutters, and outer gutters</P>
      <br>
      <br>
      <p class="content__para__hang">
      <b>Step 6:</b> Set breakpoints for smaller device sizes</P>
      <br>
      <br>
      <h4 class="content__para">Thanks for reading!</h4>
      <br>
      <br>
      <br>
      <br>
  </article>
</div>
  
    <script  src="js/article.js"></script>

</body>
</html>
